package com.brazhnik.gltest;

/**
 * Created by 6epreu on 03.12.2015.
 */
import android.content.Context;
import android.opengl.GLES20;


public class ShaderUtils {

    /**
     * Создаем OpenGL программу
     * @param vertexShaderId - Gl ID шейдера вершин
     * @param fragmentShaderId - Gl ID шейдера фрагментов
     * @return - Gl ID скомпилированной программы или 0 в случае неудачи
     */
    public static int createProgram(int vertexShaderId, int fragmentShaderId) {

        final int programId = GLES20.glCreateProgram();
        if (programId == 0) {
            return 0;
        }

        GLES20.glAttachShader(programId, vertexShaderId);
        GLES20.glAttachShader(programId, fragmentShaderId);

        GLES20.glLinkProgram(programId);
        final int[] linkStatus = new int[1];
        GLES20.glGetProgramiv(programId, GLES20.GL_LINK_STATUS, linkStatus, 0);
        if (linkStatus[0] == 0) {
            GLES20.glDeleteProgram(programId);
            return 0;
        }
        return programId;

    }

    /**
     * Компилирует GL шейдер
     * @param context
     * @param type - тип ( GLES20.GL_VERTEX_SHADER | GLES20.GL_FRAGMENT_SHADER)
     * @param shaderRawId - id ресурса из папки raw
     * @return -Gl ID скомпилированного шейдера
     */
    static int createShader( Context context, int type, int shaderRawId ) {
        String shaderText = FileUtils
                .readTextFromRaw(context, shaderRawId);
        return ShaderUtils.createShader(type, shaderText);
    }

    static int createShader( int type, String shaderText ) {
        final int shaderId = GLES20.glCreateShader(type);
        if (shaderId == 0) {
            return 0;
        }
        GLES20.glShaderSource(shaderId, shaderText);
        GLES20.glCompileShader(shaderId);
        final int[] compileStatus = new int[1];
        GLES20.glGetShaderiv(shaderId, GLES20.GL_COMPILE_STATUS, compileStatus, 0);
        if (compileStatus[0] == 0) {
            GLES20.glDeleteShader(shaderId);
            return 0;
        }
        return shaderId;
    }

}
