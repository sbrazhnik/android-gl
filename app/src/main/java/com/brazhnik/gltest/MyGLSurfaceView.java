package com.brazhnik.gltest;

import android.content.Context;
import android.opengl.GLSurfaceView;

/**
 * Created by 6epreu on 02.12.2015.
 */
class MyGLSurfaceView extends GLSurfaceView {

    private final StartAndroidGlRenderer mRenderer;

    public MyGLSurfaceView(Context context){
        super(context);

        // Create an OpenGL ES 2.0 context
        setEGLContextClientVersion(2);

        mRenderer = new StartAndroidGlRenderer(getContext());

        // Set the Renderer for drawing on the GLSurfaceView
        setRenderer(mRenderer);
    }
}