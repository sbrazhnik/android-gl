package com.brazhnik.gltest;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by 6epreu on 03.12.2015.
 */
public class StartAndroidGlRenderer implements GLSurfaceView.Renderer {

    private Context context;
    private FloatBuffer vertexBuf;
    private int programId;
    private int uColorLocation;
    private int aPositionLocation;

    public StartAndroidGlRenderer(Context context) {
        this.context = context;
        prepareData();
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        GLES20.glClearColor(0, 0, 0, 1f);
        int vertexShaderId = ShaderUtils.createShader(context, GLES20.GL_VERTEX_SHADER, R.raw.vertex_shader);
        int fragmentShaderId = ShaderUtils.createShader(context, GLES20.GL_FRAGMENT_SHADER, R.raw.fragment_shader);
        programId = ShaderUtils.createProgram(vertexShaderId, fragmentShaderId);
        GLES20.glUseProgram(programId);
        bindData();
    }

    /**
     * готовим данные для отрисовки
     * 3 координаты треугольника и выделяем под них память
     */
    private void prepareData()
    {
        float[] vertices = {
                // треугольник 1
                -0.9f, 0.8f,
                -0.9f, 0.2f,
                -0.5f, 0.8f,

                // треугольник 2
                -0.6f, 0.2f,
                -0.2f, 0.2f,
                -0.2f, 0.8f,

                // треугольник 3
                0.1f, 0.8f,
                0.1f, 0.2f,
                0.5f, 0.8f,

                // треугольник 4
                0.1f, 0.2f,
                0.5f, 0.2f,
                0.5f, 0.8f,

                // линия 1
                -0.7f, -0.1f,
                0.7f, -0.1f,

                // линия 2
                -0.6f, -0.2f,
                0.6f, -0.2f,

                // точка 1
                -0.5f, -0.3f,

                // точка 2
                0.0f, -0.3f,

                // точка 3
                0.5f, -0.3f,
        };

        vertexBuf = ByteBuffer.allocateDirect(vertices.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        vertexBuf.put(vertices);
    }

    /**
     * Передаем данные в шейдер
     */
    private void bindData(){
        // ссылка на переменную u_Color во фрагментном шейдере
        uColorLocation = GLES20.glGetUniformLocation( programId, "u_Color" );
        // передает в шейдер значение цвета
        GLES20.glUniform4f(uColorLocation, 1.0f, 0.0f, 0.1f, 1.0f);

        // ссылка на переменную a_Position в вершинном шейдере
        aPositionLocation = GLES20.glGetAttribLocation(programId, "a_Position");
        // данные из vertexData надо будет читать начиная с элемента с индексом 0, т.е. с самого начала.
        vertexBuf.position(0);

        // int indx – переменная указывающая на положение атрибута в шейдере
        // int size – указывает системе, сколько элементов буфера vertexData брать для заполнения атрибута a_Position. !!!! отличное описание этого параметра на startandroid
        // int type – передаем GL_FLOAT, т.к. у нас float значения
        // boolean normalized – этот флаг нам пока неактуален, ставим false
        // int stride – используется при передаче более чем одного атрибута в массиве
        // Buffer ptr – буффер с данными, т.е. vertexData.
        GLES20.glVertexAttribPointer(aPositionLocation, 2, GLES20.GL_FLOAT,
                false, 0, vertexBuf);

        //  включить атрибут aPositionLocation
        GLES20.glEnableVertexAttribArray(aPositionLocation);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        // заливаем экран дефолтным цветом
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

        GLES20.glLineWidth(5);

        // int mode – какой тип графических примитивов хотим рисовать
        // int first – указываем, что брать вершины из массива вершин надо начиная с элемента с индексом 0, т.е. с первого элемента массива
        // int count – кол-во вершин которое необходимо использовать для рисования. Указываем 3
        GLES20.glUniform4f(uColorLocation, 1.0f, 0.0f, 0.1f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 12);

        GLES20.glUniform4f(uColorLocation, 0.0f, 1.0f, 0.1f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_LINES, 12, 4);

        GLES20.glUniform4f(uColorLocation, 0.0f, 0.0f, 1.0f, 1.0f);
        GLES20.glDrawArrays(GLES20.GL_POINTS, 16, 3);
    }
}
