// vec4 – тип данных вектор из 4 float значений, именно 4 значения, тк 3 координаты ( x,y,z ) и еще какой то
// аттрибут
// a_Position - переменная в которой будет храниться текущая координата ( ПЕРЕДАЕТСЯ ИЗ ПРИЛОЖЕНИЯ )
attribute vec4 a_Position;
attribute vec4 a_Color;

varying vec4 v_Color;

void main() // с этого метода начинается выполнение шейдера
{
    // gl_Position - переменная OpenGl которая является результатом работы вершинного шейдера
    gl_Position = a_Position;
    gl_PointSize = 15.0;

    v_Color = a_Color;
}